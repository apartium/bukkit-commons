package net.apartium.dev.bukkitcommons;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class VersionUtils {

	private final static String
		PACKAGENAME = Bukkit.getServer().getClass().getPackage().getName(),
		VERSION = PACKAGENAME.substring(PACKAGENAME.lastIndexOf(".") + 1);

	
	private static Method
			GET_VERSION;
	
	static {
		try {
			GET_VERSION = NMSUtils.NMS_PLAYERCONNECTION.getMethod("getVersion");
		} catch (NoSuchMethodException | SecurityException e) {

		}
	}
	
	/**
	 * Get the minecraft version
	 * @return The minecraft version of the server.
	 */
	public static String getVersion() {
		return VERSION;
	}
	
	/**
	 * Get the protocol version of a specific player.
	 * @param player The specified player.
	 * @return The protcol version number of the player.
	 */
	public static int getProtocolVersion(Player player) {
		try {
			Object ins = PlayerUtils.getNetworkManager(player);
			if (ins == null) return 0;
			if (GET_VERSION == null) return 0;
			return (int) GET_VERSION.invoke(ins);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
}
