package net.apartium.dev.bukkitcommons;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.entity.Player;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class DamageUtils {

	private final static Class<?>
			NMS_DAMAGESOURCE = NMSUtils.getClassInternal(true, "DamageSource");
	
	/**
	 * Get damage source of NMS.
	 * @param source the name of the damage source
	 * @return the object presenting the damage source
	 */
	public static Object getNMSDamageSource(String source) {
		Validate.notEmpty(source, "source +-");
		try {
			Field field = NMS_DAMAGESOURCE.getField(source.toUpperCase());
			return field.get(null);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Kill a player using NMS (EntityPlayer#die)
	 * @param player specified player
	 */
	public static void kill(Player player) {
		Validate.notNull(player, "player +-");
		Object source = getNMSDamageSource("GENERIC");
		if (source == null)
			return;
		
		try {
			Method kill = NMSUtils.NMS_PLAYER.getMethod("die", NMS_DAMAGESOURCE);
			kill.invoke(NMSUtils.getHandle(player), source);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
	}

}
