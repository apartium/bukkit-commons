package net.apartium.dev.bukkitcommons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class OfflinePlayerUtils {

	/**
	 * Convert a collection of uuid's to collection of offlineplayer
	 * @param uuids the collection of uuids
	 * @return collection of offlineplayer
	 */
	public static Collection<OfflinePlayer> convertCollection(Collection<UUID> uuids) {
		Validate.notEmpty(uuids, "uuid +-");
		
		List<OfflinePlayer> list = new ArrayList<>();
		for (UUID uuid : uuids)
			list.add(Bukkit.getOfflinePlayer(uuid));
		
		return list;
	}
}
