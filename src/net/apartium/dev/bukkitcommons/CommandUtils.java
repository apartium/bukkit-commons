package net.apartium.dev.bukkitcommons;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class CommandUtils {

	/**
	 * get a list of players matching the specified argument of vanilla target selection
	 * @param sender the sender of the command
	 * @param arg the target argument (vanilla style)
	 * @return a array of players matching the arguments
	 */
	public static Player[] getPlayers(CommandSender sender, String arg) {
		Validate.notNull(sender, "sender +-");
		Validate.notEmpty(arg, "arg +-");

		Object handle = NMSUtils.getHandle(sender);
		try {
			Method getPlayers;
			try {
				getPlayers = NMSUtils.getClass(true, "PlayerSelector").getMethod("getPlayers", NMSUtils.getClass(true, "ICommandListener"),
						String.class);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return new Player[0];
			}
			Method getName = NMSUtils.NMS_HUMAN.getMethod("getName");

			List<Player> list = new ArrayList<>();
			for (Object obj : (Object[]) getPlayers.invoke(null, handle, arg))
				list.add(Bukkit.getPlayer((String) getName.invoke(obj)));

			return list.toArray(new Player[list.size()]);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}

	}
}