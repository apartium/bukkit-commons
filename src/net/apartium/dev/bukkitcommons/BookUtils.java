package net.apartium.dev.bukkitcommons;

import java.lang.reflect.Method;
import java.util.List;
import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.commons.lang.reflect.MethodUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class BookUtils {
	
	static {
		try {
			openBook = NMSUtils.NMS_PLAYER.getMethod("a",
					new Class[] { NMSUtils.getClass(true, "ItemStack"), NMSUtils.getClass(true, "EnumHand") });
		} catch (NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static Method openBook;

	/**
	 * Open a book for a player.
	 * @param player The player to open the book to.
	 * @param book The book to open.
	 */
	public static void openBook(Player player, ItemStack book) {
		Validate.notNull(player, "player +-");
		Validate.notNull(book, "book +-");
		
		if (!(book.getItemMeta() instanceof BookMeta))
			return;
		
		ItemStack held = player.getInventory().getItemInMainHand();
		
		try {
			player.getInventory().setItemInMainHand(book);
			Object entityPlayer = NMSUtils.getHandle(player);
			Class<?> enumHand = NMSUtils.getClass(true, "EnumHand");
			Object[] enumArray = enumHand.getEnumConstants();
			openBook.invoke(entityPlayer, new Object[] { ItemUtils.asNMSCopy(book), enumArray[0] });
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		
		player.getInventory().setItemInMainHand(held);
	}
	
	/**
	 * Set the content of a book
	 * @param metadata The book's meta.
	 * @param pages The pages to set
	 */
	public static void setPages(BookMeta metadata, List<String> pages) {
		try {
			@SuppressWarnings("unchecked")
			List<Object> p = (List<Object>) FieldUtils.getField(NMSUtils.getClass(false, "inventory.CraftMetaBook"), "pages")
					.get(metadata);
			for (String text : pages) {
				Object page = MethodUtils.invokeMethod(
						NMSUtils.getClass(true, "IChatBaseComponent$ChatSerializer").newInstance(), "a", text);
				p.add(page);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
