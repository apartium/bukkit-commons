package net.apartium.dev.bukkitcommons;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

import net.apartium.dev.javacommons.ObjectUtils;

public class EntityUtils {
	private static final Map<Entity, Listener> godListeners = new HashMap<>();
	private static final Map<Entity, Listener> harmListeners = new HashMap<>();

	/**
	 * Set this entity as invulnerable (Will not be able to take any damage)
	 * @param plugin The plugin to register the required listener to
	 * @param entity The entity to set mode to
	 * @param mode True for make the entity not able to take damage, false to reverse it
	 */
	public static void setInvulnerable(Plugin plugin, Entity entity, boolean mode) {
		ObjectUtils.Validate.notNull(entity, "entity +-");
		if (mode) {
			ObjectUtils.Validate.notNull(plugin, "plugin +-");

			Listener listener = new Listener() {
				@EventHandler
				public void onDamage(EntityDamageEvent e) {
					if (e.getEntity().equals(entity)) 
						e.setCancelled(true);
					
				}
			};

			Bukkit.getPluginManager().registerEvents(listener, plugin);
		} else if (godListeners.containsKey(entity))
			EntityDamageEvent.getHandlerList().unregister((Listener) godListeners.get(entity));
		
	}

	/**
	 * Set field of an entity
	 * @param entity The bukkit entity
	 * @param nms Should the field be set on NMS or OBC: true for NMS, false for OBC
	 * @param fieldName The name of the field
	 * @param value The new value to set
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException
	 */
	public static void setField(Entity entity, boolean nms, String fieldName, Object value)
			throws IllegalArgumentException, NoSuchFieldException {
		ObjectUtils.Validate.notNull(entity, "entity +-");
		ObjectUtils.Validate.notNull(value, "value +-");
		ObjectUtils.Validate.notEmpty(fieldName, "fieldName +-");
		
		Object instance;
		Class<?> clazz;

		if (nms) 
			instance = NMSUtils.getHandle(entity);
		else 
			instance = entity;
		
		clazz = instance.getClass();
		
		Field field;
		
		try {
			field = clazz.getField(fieldName);
		} catch (NoSuchFieldException e) {
			try {
				field = clazz.getDeclaredField(fieldName);
			} catch(NoSuchFieldException e1) {
				throw e1;
			}
		}
		
		boolean acc = field.isAccessible();
		field.setAccessible(true);
		
		try {
			field.set(instance, value);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		if (!acc) 
			field.setAccessible(false);
		
	}

	/**
	 * Get the value of a field of entity
	 * @param entity The bukkit entity
	 * @param nms Is the field from NMS or OBC: true for NMS, false for OBC
	 * @param fieldName The name of the field
	 * @return The field's value
	 * 
	 * @throws NoSuchFieldException
	 */
	public static Object getFieldValue(Entity entity, boolean nms, String fieldName) 
			throws NoSuchFieldException {
		ObjectUtils.Validate.notNull(entity, "player +-");
		ObjectUtils.Validate.notEmpty(fieldName, "fieldName +-");
		
		Object instance;
		Class<?> clazz;
		
		if (nms) 
			instance = NMSUtils.getHandle(entity);
		else 
			instance = entity;
		
		clazz = instance.getClass();
		
		Field field;
		try {
			field = clazz.getField(fieldName);
		} catch (NoSuchFieldException e) {
			try {
				field = clazz.getDeclaredField(fieldName);
			} catch(NoSuchFieldException e1) {
				throw e1;
			}
		}
		
		boolean acc = field.isAccessible();
		field.setAccessible(true);

		Object value = null;
		try {
			value = field.get(instance);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		if (!acc) 
			field.setAccessible(false);
		
		return value;
	}

	/**
	 * Set if this entity can harm.
	 * NOTE: This will not modify the AI for the specified entity.
	 * @param plugin The plugin to register the required listener to. not required if the value is false.
	 * @param entity The entity.
	 * @param value True for can harm, else false.
	 */
	public static void setCanHarm(Plugin plugin, Entity entity, boolean value) {
		ObjectUtils.Validate.notNull(entity, "entity +-");
		
		if (value) {
			Listener listener = (Listener) harmListeners.get(entity);
			if (listener == null) 
				return;
			
			EntityDamageByEntityEvent.getHandlerList().unregister(listener);
			
		} else {
			ObjectUtils.Validate.notNull(plugin, "plugin +-");

			Listener listener = new Listener() {
				@EventHandler
				public void onDamage(EntityDamageByEntityEvent e) {
					if (e.getDamager().equals(entity)) {
						e.setCancelled(true);
					}
				}
			};
			
			Bukkit.getPluginManager().registerEvents(listener, plugin);
		}
	}
}
