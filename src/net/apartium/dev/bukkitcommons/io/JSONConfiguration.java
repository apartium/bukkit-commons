package net.apartium.dev.bukkitcommons.io;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class JSONConfiguration extends FileConfiguration {
	
	@Override
	protected String buildHeader() {

		return "";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadFromString(String arg0) throws InvalidConfigurationException {
		Validate.notEmpty(arg0, "arg0 +-");
		
		try {
			super.map.putAll((Map<? extends String, ? extends Object>) 
					new JSONParser().parse(arg0));
		} catch (ParseException e) {
			throw new InvalidConfigurationException("Invalid json format");
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String saveToString() {
		JSONObject object = new JSONObject();
		object.putAll(super.map);
		return object.toJSONString();
	}
	
	public static JSONConfiguration loadConfiguration(File file) {
		Validate.notNull(file, "file cannot be null");
		
		JSONConfiguration config = new JSONConfiguration();
		try {
			config.load(file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		
		return config;
	}
}
