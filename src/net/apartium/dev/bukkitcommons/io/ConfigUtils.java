package net.apartium.dev.bukkitcommons.io;

import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class ConfigUtils {

	/**
	 * get UUID from an object
	 * @param object the object to try get a UUID from
	 * @return null if failed. if succed, the UUID
	 */
	public static UUID getUniqueId(Object object) {
		Validate.notNull(object, "object +-");
		
		if (object instanceof UUID)
			return (UUID) object;
		
		UUID uuid = UUID.fromString(object.toString());
		return uuid;
	}
	
	/**
	 * Get OfflinePlayer from an object
	 * @param object the object to try get the OfflinePlayer from
	 * @return null if failed. if succed, the OfflinePlayer
	 */
	@SuppressWarnings("deprecation")
	public static OfflinePlayer getOfflinePlayer(Object object) {
		Validate.notNull(object, "object +-");

		if (object instanceof OfflinePlayer)
			return (OfflinePlayer) object;
		
		UUID uuid = getUniqueId(object);
		if (uuid != null)
			return Bukkit.getOfflinePlayer(uuid);
		
		return Bukkit.getOfflinePlayer(object.toString());
	}
}
