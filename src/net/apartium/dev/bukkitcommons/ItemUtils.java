package net.apartium.dev.bukkitcommons;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.apartium.dev.javacommons.ObjectUtils;
import org.apache.commons.lang.reflect.MethodUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemUtils {
	public static final List<Material> helmets = Arrays.asList(new Material[] { Material.LEATHER_HELMET,
			Material.CHAINMAIL_HELMET, Material.IRON_HELMET, Material.GOLD_HELMET, Material.DIAMOND_HELMET });
	public static final List<Material> chestplates = Arrays
			.asList(new Material[] { Material.LEATHER_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE,
					Material.IRON_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.IRON_CHESTPLATE });
	public static final List<Material> leggings = Arrays.asList(new Material[] { Material.LEATHER_LEGGINGS,
			Material.CHAINMAIL_LEGGINGS, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.DIAMOND_LEGGINGS });
	public static final List<Material> boots = Arrays.asList(new Material[] { Material.LEATHER_BOOTS,
			Material.CHAINMAIL_BOOTS, Material.GOLD_BOOTS, Material.GOLD_CHESTPLATE, Material.DIAMOND_BOOTS });
	static Method asNMSCopy;

	public static enum ArrmorPart {
		HELMET(ItemUtils.helmets), CHESTPLATE(ItemUtils.chestplates), LEGGINGS(ItemUtils.leggings), BOOTS(
				ItemUtils.boots);

		private List<Material> materials;

		private ArrmorPart(List<Material> materials) {
			this.materials = materials;
		}

		public boolean is(Material material) {
			return this.materials.contains(material);
		}

		public boolean is(ItemStack itemStack) {
			return this.materials.contains(itemStack.getType());
		}
	}

	static {
		try {
			asNMSCopy = MethodUtils.getAccessibleMethod(NMSUtils.getClass(false, "inventory.CraftItemStack"),
					"asNMSCopy", ItemStack.class);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create skull item of a player.
	 * @param displayName The name of the skull item
	 * @param skullOwnerName The name of the skull owner which this skull should take the skin from.
	 * @return The itemstack instance of the skull item
	 */
	public static ItemStack createPlayerSkull(String displayName, String skullOwnerName) {
		ObjectUtils.Validate.notEmpty(skullOwnerName, "skullOwnerName +-");

		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta meta = (SkullMeta) item.getItemMeta();

		meta.setOwner(skullOwnerName);
		
		if (displayName != null)
			meta.setDisplayName(displayName);

		item.setItemMeta(meta);

		return item;
	}

	/**
	 * Create NMS copy of an ItemStack using CraftItemStack#asNMSCopy(ItemStack)
	 * @param is The item stack to copy
	 * @return The NMS ItemStack object.
	 */
	public static Object asNMSCopy(ItemStack is) {
		try {
			return asNMSCopy.invoke(null, new Object[] { is });
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Check if the specified material is helmet
	 * @param type the material to check.
	 * @return true if the specified material is helmet, else false
	 */
	public static boolean isHelmet(Material type) {
		return helmets.contains(type);
	}

	/**
	 * Check if the specific item is 
	 * @param item The specified item.
	 * @return true if the specified item is {}, else false
	 */
	public static boolean isHelmet(ItemStack item) {
		return isHelmet(item.getType());
	}
	
	/**
	 * Check if the specified material is chestplate
	 * @param type the material to check.
	 * @return true if the specified material is chestplate, else false
	 */
	public static boolean isChestplate(Material type) {
		return chestplates.contains(type);
	}

	/**
	 * Check if the specific item is chestplate
	 * @param item The specified item.
	 * @return true if the specified item is chestplate, else false
	 */
	public static boolean isChestplate(ItemStack item) {
		return isChestplate(item.getType());
	}
	
	/**
	 * Check if the specified material is leggings
	 * @param type the material to check.
	 * @return true if the specified material is leggins, else false
	 */
	public static boolean isLeggings(Material type) {
		return leggings.contains(type);
	}

	/**
	 * Check if the specific item is leggings
	 * @param item The specified item.
	 * @return true if the specified item is leggings, else false
	 */
	public static boolean isLeggings(ItemStack item) {
		return isLeggings(item.getType());
	}
	
	/**
	 * Check if the specified material is boots
	 * @param type the material to check.
	 * @return true if the specified material is boots, else false
	 */
	public static boolean isBoots(Material type) {
		return boots.contains(type);
	}

	/**
	 * Check if the specific item is boots
	 * @param item The specified item.
	 * @return true if the specified item is boots, else false
	 */
	public static boolean isBoots(ItemStack item) {
		return isBoots(item.getType());
	}

	public static class ItemCollections {
		public static List<ItemStack> filter(List<ItemStack> base, ItemUtils.ArrmorPart arrmorPart) {
			List<ItemStack> results = new ArrayList<>();
			for (ItemStack is : base) {
				if (arrmorPart.is(is)) {
					results.add(is);
				}
			}
			return results;
		}

		public static List<ItemStack> filter(ItemStack[] base, ItemUtils.ArrmorPart arrmorPart) {
			return filter(Arrays.asList(base), arrmorPart);
		}
	}
}
