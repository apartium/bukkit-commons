package net.apartium.dev.bukkitcommons;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import net.apartium.dev.javacommons.StringUtils;

import org.apache.commons.lang.Validate;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Fetcher {

	private static final String
		PROFILE_URL = "https://api.mojang.com/user/profiles/";

	private static final JSONParser
		jsonParser = new JSONParser();
	
	/**
	 * Get UUID by player name
	 * @param name the name of the player
	 * @return the UUID associated with the name 
	 * @throws IOException 
	 */
	public static UUID getUUID(String name) throws IOException {	
		Validate.notEmpty(name, "name +-");
		
		//if (Bukkit.getPlayer(name) != null)
			//return Bukkit.getPlayer(name).getUniqueId();
		
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(PROFILE_URL + name).openConnection();
			JSONObject response = (JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			String uuid = (String) response.get("id");
			if (uuid == null) 
				return null;
			
			return UUID.fromString(StringUtils.UUIDUtils.addHyphens(uuid));
		}		
		catch (ParseException e) {
			//ChatUtils.sendConsoleError(ErrorType.FETCHER, e, "UUIDFetcherFail", "Parse");
			return null;
		}
	}
	
	/**
	 * Get the name history of a player.
	 * @param uuid the UUID of the player.
	 * @return the name history of the player.
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	public static List<Entry<String, Date>> getNames(UUID uuid) throws IOException {
		Validate.notNull(uuid, "uuid +-");
		
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(PROFILE_URL + uuid.toString().replace("-", "") + "/names").openConnection();
			List<JSONObject> response = (List<JSONObject>) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			
			List<Entry<String, Date>> names = new ArrayList<>();
			for (JSONObject obj : response) {
				String name = (String) obj.get("name");
				Date date = null;
				if (obj.get("changedToAt") != null)
					date = new Date((long) obj.get("changedToAt"));
				
				names.add(new SimpleEntry<>(name, date));
			}
			
			return names;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Get the current name of a player.
	 * @param uuid the UUID of the player.
	 * @return the current name of the player.
	 * @throws IOException 
	 */
	public static String getName(UUID uuid) throws IOException {
		Validate.notNull(uuid, "uuid +-");
		
		List<Entry<String, Date>> names = getNames(uuid);
		if (names.size() == 0)
			return null;
		return names.get(names.size() - 1).getKey();
		
	}
}