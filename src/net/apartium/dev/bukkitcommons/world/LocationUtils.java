package net.apartium.dev.bukkitcommons.world;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class LocationUtils {

	/**
	 * Checking if the specified locations have the same XYZ.
	 * @param loc1 the first location.
	 * @param loc2 the second location.
	 * @return if the location have the same XYZ.
	 */
	public static boolean sameXYZ(Location loc1, Location loc2, Location...locations) {
		Validate.notNull(loc1, "loc1 +-");
		Validate.notNull(loc2, "loc2 +-");
		Validate.notEmpty(locations, "location +-");
		
		if (loc1 == null || loc2 == null)
			return false;
		
		double x = loc1.getX();
		double y = loc1.getY();
		double z = loc1.getZ();
		
		if (loc2.getX() != x || loc2.getY() != y || loc2.getZ() != z)
			return false;
		
		for (Location loc : locations)
			if (loc.getX() != x || loc.getY() != y || loc.getZ() != z)
				return false;
		
		return true;
	}
	
	/**
	 * Checking if the locations is in the same world
	 * @param loc1
	 * @param loc2
	 * @return
	 */
	public static boolean sameWorld(Location loc1, Location loc2) {
		Validate.notNull(loc1, "loc1 +-");
		Validate.notNull(loc2, "loc2 +-");

		if (loc1 == null || loc2 == null)
			return false;
		
		return loc1.getWorld().equals(loc2.getWorld());
	}
	
	/**
	 * Paste non relative map
	 * @param blocks the map containing the blocks
	 */
	@SuppressWarnings("deprecation")
	public static void pasteRaw(Map<Location,Entry<Material,Byte>> blocks) {
		for (Entry<Location,Entry<Material,Byte>> entry : blocks.entrySet()) {
			Block block = entry.getKey().getBlock();
			block.setType(entry.getValue().getKey());
			block.setData(entry.getValue().getValue());
			
		}
	}
	
	//public static void paste(Map<RelativeLocation, BlockData> blocks, World world) {
	//	Map<Location,Entry<Material,Byte>> rawBlockData = new HashMap<>();
	//	for (Entry<RelativeLocation, BlockData> entry : blocks.entrySet())
	//		rawBlockData.put(entry.getKey().clone(world), new AbstractMap.SimpleEntry(entry.getValue().getType(),
	//				entry.getValue().getData()));
		
	//	pasteRaw(rawBlockData);
	//}
	
	public static Set<Location> getLocationsBetween(Location pos1, Location pos2) {
		Validate.notNull(pos1, "pos1 +-");
		Validate.notNull(pos2, "pos2 +-");

		Set<Location> set = new HashSet<>();
		
		int maxX, minX, maxY, minY, maxZ, minZ;
		World world = pos1.getWorld();
		
		if (pos1.getBlockX() > pos2.getBlockX()) {
			maxX = pos1.getBlockX();
			minX = pos2.getBlockX();
		} else {
			maxX = pos2.getBlockX();
			minX = pos1.getBlockX();
		}
		
		if (pos1.getBlockZ() > pos2.getBlockZ()) {
			maxZ = pos1.getBlockZ();
			minZ = pos2.getBlockZ();
		} else {
			maxZ = pos2.getBlockZ();
			minZ = pos1.getBlockZ();
		}
		
		if (pos1.getBlockY() > pos2.getBlockY()) {
			maxY = pos1.getBlockY();
			minY = pos2.getBlockY();
		} else {
			maxY = pos2.getBlockY();
			minY = pos1.getBlockY();
		}

		
		for (int x = minX; x < maxX; x++) 
			for (int y = minY; y < maxY; y++) 
				for (int z = minZ; z < maxZ; z++) 
					set.add(new Location(world, x, y, z));
		
		return set;
	}
	
}