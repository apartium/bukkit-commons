package net.apartium.dev.bukkitcommons.world;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class SoundUtils {
	
	/**
	 * Play a sound in specific location.
	 * 
	 * @param location the location to play to sound in
	 * @param sound the sound to play
	 * @param volume the volume of the sound
	 * @param pitch the pitch of the sound
	 * @param radius the radius of the sound
	 */
	public static void playSound(Location location, Sound sound, int volume, int pitch, double radius) {
		Validate.notNull(location, "location +-");
		Validate.notNull(sound, "sound +-");

		for (Player player : location.getWorld().getPlayers())
			if (player.getLocation().distance(location) <= radius)
				player.playSound(location, sound, volume, pitch);
	
	}
	
}