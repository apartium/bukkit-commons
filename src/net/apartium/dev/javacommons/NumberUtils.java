package net.apartium.dev.javacommons;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.reflect.MethodUtils;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class NumberUtils {

	@SuppressWarnings("unchecked")
	public static <T extends Number> T valueOf(String arg, Class<T> clazz) {
		Validate.notEmpty(arg, "arg +-");
		Validate.notNull(clazz, "clazz +-");
		try {
			return (T) MethodUtils.invokeStaticMethod(clazz, "valueOf", new Object[] {arg});
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}
}
