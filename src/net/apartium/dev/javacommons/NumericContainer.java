package net.apartium.dev.javacommons;

public class NumericContainer<T extends Number>{

	final T
			number;
	
	final boolean
			has;
	
	public NumericContainer() {
		this.number = null;
		this.has = false;
		
	}
	
	public NumericContainer(T number) {
		this.number = number;
		this.has = true;
	}
	
	public boolean isNull() {
		return has;
	}
	
	public T getNumber() {
		return number;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (has && number.equals(obj))
			return true;
		
		return super.equals(obj);
	}
	
}
