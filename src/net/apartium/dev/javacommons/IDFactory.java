package net.apartium.dev.javacommons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class IDFactory {

	private static volatile Map<String,IDFactory>
			ids = new ConcurrentHashMap<>();
	
	private static IDFactory ins = new IDFactory("____________-________-________-________");
	
	public static IDFactory getDefaultFactory() {return ins;}
	
	protected String
			base;
	
	public IDFactory(String base) {
		this.base = base;
		
		ids.put(base, this);
	}
	
	public String getId() {
		return base;
	}
}
