package net.apartium.dev.javacommons;

import com.google.common.collect.Sets;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.reflect.FieldUtils;

public class ReflectionUtils {
	public static class FieldSearcher {
		public static List<Field> search(Class<?> clazz, Class<?> type) {
			return search(clazz, Sets.newHashSet(new Class[] { type }));
		}

		public static List<Field> search(Class<?> clazz, Set<Class<?>> types) {
			if (types == null) {
				types = new HashSet<>();
			}
			
			List<Field> fields = new ArrayList<>();
			Field[] arrayOfField;
			int j = (arrayOfField = FieldUtils.getAllFields(clazz)).length;
			for (int i = 0; i < j; i++) {
				Field field = arrayOfField[i];
				if ((types.size() == 0) || (types.contains(field.getType()))) {
					fields.add(field);
				}
			}
			return fields;
		}

		public static List<Field> search(Class<?> clazz, Set<Class<?>> types, boolean inheritance) {
			if (types == null) {
				types = new HashSet<>();
			}
			List<Field> fields = new ArrayList<>();
			for (Class<?> param : types) {
				fields.addAll(ReflectionUtils.FieldCollections.getFields(Arrays.asList(FieldUtils.getAllFields(clazz)),
						param, inheritance));
			}
			return fields;
		}

		public static boolean match(Class<?> clazz, Class<?> param, boolean inheritance) {
			return (clazz.equals(param)) || ((inheritance) && (param.isAssignableFrom(clazz)));
		}
	}

	public static class TypeCollections {
		public static List<Class<?>> getTypes(List<Field> fields) {
			List<Class<?>> results = new ArrayList<>();
			for (Field field : fields) {
				results.add(field.getType());
			}
			return results;
		}

		public static List<Class<?>> getTypes(List<Class<?>> source, Class<?> param, boolean allowInheritance) {
			List<Class<?>> results = new ArrayList<>();
			for (Class<?> clazz : source) {
				if (ReflectionUtils.FieldSearcher.match(clazz, param, allowInheritance)) {
					results.add(clazz);
				}
			}
			return results;
		}
	}

	public static class FieldCollections {
		public static List<Field> getFields(List<Field> source, Class<?> param, boolean allowInheritance) {
			List<Field> results = new ArrayList<>();
			for (Field field : source) {
				if (ReflectionUtils.FieldSearcher.match(field.getType(), param, allowInheritance)) {
					results.add(field);
				}
			}
			return results;
		}
	}

	public static class ManualConstuctor {
		public static <T> T construct(T instance, Object... fields) {
			List<Object> values = new ArrayList<>(Arrays.asList(fields));
			for (Object object : values) {
				Field result = (Field) ReflectionUtils.FieldSearcher.search(instance.getClass(), object.getClass())
						.get(0);
				result.setAccessible(true);
				if (!Modifier.isStatic(result.getModifiers())) {
					try {
						result.set(instance, object);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
			return instance;
		}

		public static <T> T construct(Class<T> clazz, Object... fields)
				throws InstantiationException, IllegalAccessException {
			return (T) construct(clazz.newInstance(), fields);
		}
	}
}
