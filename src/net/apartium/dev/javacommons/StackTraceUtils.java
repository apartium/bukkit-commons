package net.apartium.dev.javacommons;

public class StackTraceUtils {
	
	/**
	 * Get class at stack trace
	 * @param before how much invokes ago
	 * @return the required class at the stack trace
	 */
	public static Class<?> getClassAtStacktrace(int before) {
		StackTraceElement[] elements = Thread.currentThread()
				.getStackTrace();
		try {
			return Class.forName(elements[before + 1].getClassName());
		} catch (ClassNotFoundException ex) {
			return null;
		}
	}
	
	/**
	 * Get the invoker of the current method
	 * @return the invoker of the current method.
	 */
	public static Class<?> getClassAtStacktrace() {
		return getClassAtStacktrace(1);
	}

}
