package net.apartium.dev.javacommons;

import java.io.Serializable;

public class ID implements Serializable, CharSequence {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String
		id;
	
	public ID() {
		this(IDFactory.getDefaultFactory().getId());
	}
		
	public ID(String id) {
		StringBuilder sb = new StringBuilder();
		for (char c : id.toCharArray()) 
			if (c == '_') 
				sb.append(StringUtils.ALPHABET.charAt(RandomUtils.randomInt(StringUtils.ALPHABET.length() - 1)));
			else
				sb.append(c);
		
		this.id = sb.toString();
		
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof String) && !(object instanceof ID))
			return false;
		
		return this.id.equals(object.toString());
	}
	
	@Override
	public String toString() {
		return id;
	}
	
	public String getID() {
		return id;
	}
	
	public void setID(String id) {
		this.id = id;
	}
	
	public static boolean isID(String arg) {
		return arg.matches("[0-9a-z]{4}-[0-9a-z]{8}-[0-9a-z]{8}-[0-9a-z]{8}-[0-9a-z]{8}");
	}

	public static ID parse(String arg) {
		//if (!isID(arg))
		//	return null;
		return new ID(arg);
	}

	@Override
	public char charAt(int index) {
		return id.charAt(index);
	}

	@Override
	public int length() {
		return id.length();
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		StringBuilder sb = new StringBuilder();
		for (int i = start; i < end; i++) 
			sb.append(charAt(i));
		
		return sb.toString();
	}
	
}