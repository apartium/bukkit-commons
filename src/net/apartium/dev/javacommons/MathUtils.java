package net.apartium.dev.javacommons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.util.Vector;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class MathUtils {
	
	public static String toHexString(byte[] arg) {
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < arg.length; i++)
			str.append(String.format("%x", arg[i]));
		return str.toString();
	}
	
	public static String fromHexString(String arg) {
		Validate.notEmpty(arg, "arg +-");
		
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < arg.length(); i+=2)
			str.append((char) Integer.parseInt(arg.substring(i, i + 2), 16));
		return str.toString();
	}
	
	public static double round(double arg, int digits) {
		return (double) Math.round(arg * Math.pow(10, digits)) / Math.pow(10, digits);
	}
	
	/**
	 * Getting random vector.
	 * @param x1 The X minimum.
	 * @param x2 The X maximum.
	 * @param y1 The Y minimum.
	 * @param y2 The Y maximum.
	 * @param z1 The Z minimum.
	 * @param z2 The Z maximum.
	 * @return the vector value.
	 */
	public static Vector randomVector(double x1, double x2, double y1, double y2, double z1, double z2) {
		return new Vector(RandomUtils.randomDouble(x1, x2), RandomUtils.randomDouble(y1, y2), RandomUtils.randomDouble(z1, z2));
	}
	
	/**
	 * Checking if a number between two numbers.
	 * @param arg the checked number.
	 * @param arg1 the first number.
	 * @param arg2 the seconds number.
	 * @return the result.
	 */
	public static boolean isBetween(double arg, double arg1, double arg2) {
		if ((arg1 == arg2 && arg1 == arg) || (arg1 > arg2 && arg <= arg1 && arg >= arg2))
			return true;

		return arg <= arg2 && arg >= arg1;
	}
	
	/**
	 * check if specified numbers are odd
	 * @param args the integers to check
	 * @return true if one or more of the values are odd
	 */
	public static boolean isOdd(int... args) {
		for (int i : args)
			if (isDouble(i / 2))
				return true;
		
		return false;
	}
	
	/**
	 * Checking if double is a inetegr.
	 * @param arg the double argument.
	 * @return the result.
	 */
	public static boolean isInteger(double arg) {
		return arg == Math.floor(arg) && !Double.isInfinite(arg);
	}
	
	/**
	 * Checking if an argument is a byte.
	 * @param arg the checked argument.
	 * @return if the argument is a byte.
	 */
	public static boolean isByte(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Byte.parseByte(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Checking if an argument is a long.
	 * @param arg the checked argument.
	 * @return if the argument is a long.
	 */
	public static boolean isLong(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Long.parseLong(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Checking if an argument is a float.
	 * @param arg the checked argument.
	 * @return if the argument is a float.
	 */
	public static boolean isFloat(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Float.parseFloat(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Checking if an argument is a short.
	 * @param arg the checked argument.
	 * @return if the argument is a short.
	 */
	public static boolean isShort(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Short.parseShort(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Checking if an argument is an integer.
	 * @param arg the checked argument.
	 * @return if the argument is an integer.
	 */
	public static boolean isInteger(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Integer.parseInt(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Checking if an argument is an double.
	 * @param arg the checked argument.
	 * @return if the argument is an double.
	 */
	public static boolean isDouble(Object arg) {
		Validate.notNull(arg, "arg +-");

		try {
			Double.parseDouble(arg.toString());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Calculate percents on a map and returns the selected key
	 * @param map the map, K is any type of number in java
	 * @param removeSelected remove the chosen entry from the map
	 * @return the chosen key
	 */
    public static <K extends Number & Comparable<K>, V> K calculateMapPercents(Map<K,V> map, boolean removeSelected) {
    	Validate.notNull(map, "map +-");
    	
        double random = new Random().nextInt(100);
        List<K> nums = new ArrayList<>();
        for (K d : map.keySet())
            nums.add(d);
       
        Collections.sort(nums);
       
        for (K d : nums)
            if (random <= d.doubleValue()) {
                if (removeSelected)
                    map.remove(d);
                return d;
            }
       
        return null;
    }
    
	/**
	 * Calculate percents on a map and returns the selected key
	 * @param map the map, K is any type of number in java
	 * @return the chosen key
	 */
    public static <K extends Number & Comparable<K>, V> K calculateMapPercents(Map<K, V> map) {
       
        return calculateMapPercents(map, false);
    }

}