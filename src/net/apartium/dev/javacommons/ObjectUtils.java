package net.apartium.dev.javacommons;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

public class ObjectUtils {
	
	/**
	 * Create a new entry.
	 * @param key the key of the entry
	 * @param value the value of the entry
	 * @return the created entry
	 */
	public static <K,V> Entry<K,V> newEntry(K key, V value) {
		Validate.notNull(key, "key +-");
		Validate.notNull(value, "value +-");

		return new AbstractMap.SimpleEntry<>(key, value);
	}
	
	/**
	 * Check if a String is empty.
	 * @param string the string to check on
	 * @return true if empty, else false
	 */
	public static boolean isEmpty(String string) {
		return string != null && string.equals("") && string.length() > 0;
	}
	
	public static class Validate {
		
		/**
		 * Validate if a String is empty or not
		 * @param string the String to validate on
		 * @param ex the exception to throw if the String is empty
		 */
		public static void notEmpty(String string, Exception ex) {
			if (ex == null)
				ex = new Exception();
			
			if (isEmpty(string))
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		/**
		 * Check if an object is null.
		 * @param obj the object to check on
		 * @param ex the exception to throw if the object is null
		 */
		public static void notNull(Object obj, Exception ex) {
			if (ex == null)
				ex = new Exception();
			
			if (obj == null) 
				try {
					throw ex;
				} catch (Exception e) {
					//SystemPlus.sendError(ex, "Object can't be null");
				}
			
		}
		
		public static void notEmpty(Collection<?> collection, Exception ex) {
			Validate.notNull(collection, ex);
			if (collection.size() > 0)
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		}
		
		public static void notEmpty(Collection<?> collection, String message) {
			notEmpty(collection, new Exception(message.replaceAll("\\+-", "cannot be empty")));
		}
		
		public static void notEmpty(Map<?,?> map, Exception ex) {
			Validate.notNull(map, ex);
			Validate.notNull(map.entrySet(), ex);

			if (map.size() > 0)
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		}
		
		public static void notEmpty(Map<?,?> map, String message) {
			notEmpty(map, new Exception(message.replaceAll("\\+-", "cannot be empty")));
		}


		public static void notEmpty(Object[] array, Exception ex) {
			Validate.notNull(array, ex);
			
			if (array.length > 0)
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		}
		
		public static void notEmpty(Object[] array, String message) {
			notEmpty(array, new Exception(message.replaceAll("\\+-", "cannot be empty")));
		}

		/**
		 * Check if an object is null.
		 * @param obj the object to check on
		 * @param message the message to throw with the exception if the object is null
		 */
		public static void notNull(Object obj, String message) {
			notNull(obj, new Exception(message.replaceAll("\\+-", "cannot be null")));
		}

		/**
		 * Validate if a String is empty or not
		 * @param string the String to validate on
		 * @param message the message to throw with the exception if the String is empty
		 */
		public static void notEmpty(String string, String message) {
			notEmpty(string, new Exception(message.replaceAll("\\+-", "cannot be empty / null")));
		}
		
		/**
		 * Check if a number is larger than another number
		 * @param num1 the first number
		 * @param num2 the second number
		 * @param ex the exception to throw if not larger than
		 * 
		 */
		public static void largerThan(int num1, int num2, Exception ex) {
			if (num1 > num2)
				if (ex != null)
					try {
						throw ex;
					} catch (Exception e1) {
						e1.printStackTrace();
					}
		}
		
		public static void largerThan(int num1, int num2, String message) {
			largerThan(num1, num2, new Exception(message.replaceAll("\\+-", "must be larger than")));
		
		}
		
		public static void notOffline(Player player, Exception ex) {
			Validate.notNull(ex, "ex +-");
			Validate.notNull(player, ex);
			
			if (!player.isOnline())
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
		}

		public static void notOffline(Player player, String message) {
			notOffline(player, new Exception(message.replaceAll("+-", "cannot be offline")));
			
		}

	}	
	
}