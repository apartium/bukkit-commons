package net.apartium.dev.javacommons;

import java.lang.reflect.Array;
import java.util.List;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

@SuppressWarnings("unchecked")
public class ArrayUtils {

	public static <T> T[] newArray(Class<T> clazz, int length, T... values) {
		Validate.notNull(clazz, "clazz +-");
		Validate.notEmpty(values, "values +-");
		if (length < values.length)
			return null;
		
		T[] array = (T[]) Array.newInstance(clazz, length);
		for (int i = 0; i < length; i++) 
			array[i] = values[i];
		
		return array;
	}
	
	public static <T> T[] newArray(Class<T> clazz, T...values) {
		return newArray(clazz, values.length, values);
	}
	
	public static <T> T[] newArray(Class<T> clazz, List<T> list) {
		return newArray(clazz, list.toArray((T[]) Array.newInstance(clazz, list.size())));
	}
	
}
