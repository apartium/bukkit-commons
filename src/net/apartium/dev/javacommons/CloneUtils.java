package net.apartium.dev.javacommons;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.apartium.dev.javacommons.ObjectUtils.Validate;

public class CloneUtils {

	public static <T> T clone(Class<T> clazz, T base, T instance) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		Validate.notNull(clazz, "clazz +-");
		Validate.notNull(base, "base +-");
		Validate.notNull(instance, "instance +-");

		if (instance == null)
			instance = clazz.newInstance();
		
		List<Field> fields = new ArrayList<>();
		fields.addAll(Arrays.asList(clazz.getFields()));
		fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

		for (Field field : fields) 
			field.set(instance, field.get(base));
		
		return instance;
	}
}
