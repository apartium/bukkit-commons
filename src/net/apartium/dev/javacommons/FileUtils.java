package net.apartium.dev.javacommons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {
	
	public static void unzip(String pathToUpdateZip, String destinationPath) {
		byte[] byteBuffer = new byte[1024];

		try {
			new File(destinationPath).mkdirs();
			ZipInputStream inZip = new ZipInputStream(new FileInputStream(pathToUpdateZip));
			ZipEntry inZipEntry = inZip.getNextEntry();
			while (inZipEntry != null) {
				String fileName = inZipEntry.getName();
				File unZippedFile = new File(StringUtils.complete(destinationPath, File.separator) + fileName);
				if (inZipEntry.isDirectory()) {
					unZippedFile.mkdirs();
				} else {
					unZippedFile.getParentFile().mkdirs();
					unZippedFile.createNewFile();
					FileOutputStream unZippedFileOutputStream = new FileOutputStream(unZippedFile);
					int length;
					while ((length = inZip.read(byteBuffer)) > 0) {
						unZippedFileOutputStream.write(byteBuffer, 0, length);
					}
					unZippedFileOutputStream.close();
				}
				inZipEntry = inZip.getNextEntry();
			}
			// inZipEntry.close();
			inZip.close();
			System.out.println("Finished Unzipping");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}